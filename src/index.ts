import {
    setFilms, loadMore, searchEvent, startFunction
} from "./components/movie";
import { Movie } from "./enums/enums";

export async function render(): Promise<void> {
    startFunction();
    document.getElementById('popular')?.addEventListener("click", async () => {
        (<HTMLInputElement>document.getElementById('search')).value = '';
        await setFilms(Movie.Popular, 1)
    });
    document.getElementById('upcoming')?.addEventListener("click", async () => {
        (<HTMLInputElement>document.getElementById('search')).value = '';
        await setFilms(Movie.Upcoming, 1)
    });
    document.getElementById('top_rated')?.addEventListener("click", async () => {
        (<HTMLInputElement>document.getElementById('search')).value = '';
        await setFilms(Movie.TopRated, 1);
    });
    document.getElementById('submit')?.addEventListener("click", searchEvent);
    document.getElementById('load-more')?.addEventListener("click", loadMore);
}
