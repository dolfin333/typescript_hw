interface IObjectKeys {
    [key: string]: string | number | undefined | null;
}

interface IMovie extends IObjectKeys {
    poster_path: string | null,
    overview: string,
    release_date: string,
    id: number,
    original_title: string,
    original_language: string,
    title: string,
    backdrop_path: string | null
}

interface IMovies {
    results: IMovie[];
}

export { IMovies, IMovie };