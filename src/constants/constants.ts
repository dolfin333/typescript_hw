const imgPathTemplate = 'https://image.tmdb.org/t/p/original';
const RED = 'red';
const TRANSPARENT_RED = '#ff000078';

export { imgPathTemplate, RED, TRANSPARENT_RED };