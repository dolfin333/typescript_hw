import { imgPathTemplate, RED, TRANSPARENT_RED } from "../constants/constants";

class DomService {
    createFilmCard(poster_path: string | null | undefined, overview: string | undefined, release_date: string | undefined, film_id: number | undefined, isFavourite: boolean): string {
        if (!overview) {
            overview = "";
        }
        if (!release_date) {
            release_date = "";
        }
        const imgFullPath = poster_path ? imgPathTemplate + poster_path : '';
        const id_array = window.localStorage.getItem('favourites');
        let fill = '';
        if (film_id && id_array) {
            fill = id_array.indexOf(film_id.toString()) === -1 ? TRANSPARENT_RED : RED;
        }
        else {
            fill = TRANSPARENT_RED;
        }
        const main_class = isFavourite ? "col-12 p-2" : "col-lg-3 col-md-4 col-12 p-2";
        const class_card = isFavourite ? "favourite_card" : "film_card"
        return `
        <div class=${main_class}>
            <div class="card shadow-sm ${class_card}">
                <img
                    src="${imgFullPath}"
                />
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    stroke="red"
                    fill=${fill}
                    width="50"
                    height="50"
                    class="bi bi-heart-fill position-absolute p-2"
                    viewBox="0 -2 18 22"
                    film_id=${film_id}
                >
                    <path
                        fill-rule="evenodd"
                        d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                    />
                </svg>
                <div class="card-body">
                    <p class="card-text truncate">
                        ${overview}
                    </p>
                    <div
                        class="
                            d-flex
                            justify-content-between
                            align-items-center"
                    >
                        <small class="text-muted">${release_date}</small>
                    </div>
                </div>
            </div>
        </div>`
    }
    createFilmRandomCard(poster_path: string | null | undefined, overview: string | undefined, original_title: string | undefined): string {
        if (!overview) {
            overview = "";
        }
        if (!original_title) {
            original_title = "";
        }
        const imgFullPath = poster_path ? imgPathTemplate + poster_path : '';
        return `
        <div class="row py-lg-5" style="background: url(${imgFullPath}) no-repeat center center fixed; background-size: cover">
            <div class="col-lg-6 col-md-8 mx-auto" style="background-color: #2525254f">
                <h1 id="random-movie-name" class="fw-light text-light">${original_title}</h1>
                <p id="random-movie-description" class="lead text-white">
                    ${overview}
                </p>
            </div>
        </div>
        `
    }
}

export const domService = new DomService();