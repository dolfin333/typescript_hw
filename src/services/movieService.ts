import { ApiKey } from "../constants/secret";
import { Movie } from "../enums/enums";
import { IMovies, IMovie } from '../interfaces/interfaces'
import { mapper } from '../helpers/mapper'

class MovieService {
    private ApiUrlMovie = 'https://api.themoviedb.org/3/movie/'
    private ApiUrlMovieSearch = 'https://api.themoviedb.org/3/search/'
    private _nowCategory: Movie = 0;
    async getMovies(category: Movie, page: number): Promise<IMovies> {
        let endpoint = '';
        if (Movie.Popular === category) {
            endpoint = `popular?api_key=${ApiKey}&language=en-US&page=${page}`;
        }
        if (Movie.TopRated === category) {
            endpoint = `top_rated?api_key=${ApiKey}&language=en-US&page=${page}`;
        }
        if (Movie.Upcoming === category) {
            endpoint = `upcoming?api_key=${ApiKey}&language=en-US&page=${page}`;
        }
        this._nowCategory = category;
        const { results } = await (await fetch(`${this.ApiUrlMovie + endpoint}`, { method: "GET" })).json();
        const apiResult: IMovies = { results: [] };
        if (results) { apiResult.results = mapper(results); }
        return apiResult;
    }

    async getSearchMovies(page: number, query: string): Promise<IMovies> {
        const endpoint = `movie?api_key=${ApiKey}&query=${query}&page=${page}`;
        this._nowCategory = Movie.Search;
        const { results } = await (await fetch(`${this.ApiUrlMovieSearch + endpoint}`, { method: "GET" })).json();
        const apiResult: IMovies = { results: [] };
        if (results) { apiResult.results = mapper(results); }
        return apiResult;
    }

    async getMovieById(id: string | null): Promise<IMovie> {
        const endpoint = `${id}?api_key=${ApiKey}`;
        const result = await (await fetch(`${this.ApiUrlMovie + endpoint}`, { method: "GET" })).json();
        const apiResult: IMovies = { results: [] };
        if (result) { apiResult.results = mapper([result]); }
        return apiResult.results[0];
    }

    get nowCategory() {
        return this._nowCategory;
    }
}

export const movieService = new MovieService();