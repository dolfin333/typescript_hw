import { IMovie } from "../interfaces/interfaces";


function mapper(films: Record<string, unknown>[]): IMovie[] {
    const empty_film: IMovie = {
        poster_path: '',
        overview: '',
        release_date: '',
        id: -1,
        original_title: '',
        original_language: '',
        title: '',
        backdrop_path: ''
    };
    films = films.map(film => {
        const interfaceProperties = Object.keys(empty_film);
        const new_film = Object.assign({}, film);

        for (const property of Object.keys(film)) {
            if (interfaceProperties.indexOf(property) < 0) {
                delete new_film[property];
            }
        }
        return new_film as IMovie;
    })
    return films as IMovie[];
}

export { mapper }