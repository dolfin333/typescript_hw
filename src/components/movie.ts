import { movieService } from "../services/movieService";
import { domService } from "../services/domService";
import { IMovies } from '../interfaces/interfaces';
import { Movie } from "../enums/enums";
import { imgPathTemplate, RED, TRANSPARENT_RED } from "../constants/constants";

async function AddEventListenerFavourite(): Promise<void> {
    document.querySelectorAll("svg.bi-heart-fill").forEach(element => {
        element.addEventListener("click", favouriteEvent);
    });
}

async function DeleteEventListenerFavourite(): Promise<void> {
    document.querySelectorAll("svg.bi-heart-fill").forEach(element => {
        element.removeEventListener("click", favouriteEvent);
    });
}

async function startFunction(): Promise<void> {
    const movies = await setFilms(Movie.Popular, 1);
    setRandomMovie(movies);
    await setFavouriteMovies();
    DeleteEventListenerFavourite();
    AddEventListenerFavourite();
}

function setRandomMovie(movies: IMovies): void {
    if (movies.results) {
        const rand_movie = movies.results[Math.floor(Math.random() * movies.results?.length)];
        const random_film_card = domService.createFilmRandomCard(
            imgPathTemplate + rand_movie.poster_path,
            rand_movie.overview,
            rand_movie.original_title);
        const random_film_card_contaner = document.getElementById('random-movie');
        if (random_film_card_contaner) {
            random_film_card_contaner.innerHTML = random_film_card;
        }
        else {
            console.error('No html element with id random-movie');
        }
    }
}

async function setFavouriteMovies(): Promise<void> {
    const id_array_string = window.localStorage.getItem('favourites');
    const favourite_movies_container = document.getElementById('favorite-movies');
    if (id_array_string) {
        const id_array = JSON.parse(id_array_string);
        let film_cards = '';
        await Promise.all(id_array.map(async (id: string) => {
            const film = await movieService.getMovieById(id);
            const film_card = domService.createFilmCard(film.poster_path,
                film.overview,
                film.release_date,
                +id,
                true);
            film_cards += film_card;
        }));
        if (favourite_movies_container) {
            favourite_movies_container.innerHTML += film_cards;
        }
        else {
            console.error('No html element with id favorite-movies');
        }
    }
}




async function favouriteEvent(this: HTMLElement): Promise<void> {
    const id = this.getAttribute('film_id');
    const id_array = window.localStorage.getItem('favourites');
    const favorite_movies_container = document.getElementById('favorite-movies');
    let new_id_array: string[];
    const svgs = document.querySelectorAll<HTMLElement>(`[film_id="${id}"]`);
    DeleteEventListenerFavourite();
    if (this.getAttribute('fill') === RED) {
        svgs.forEach(svg => svg.setAttribute('fill', TRANSPARENT_RED))
        if (id) {
            if (id_array) {
                new_id_array = JSON.parse(id_array);
                new_id_array.splice(new_id_array.indexOf(id), 1);
                window.localStorage.setItem('favourites', JSON.stringify(new_id_array));
            }
            svgs.forEach(svg => {
                const parent = <HTMLElement>svg.parentNode;
                if (parent.classList.contains('favourite_card')) {
                    favorite_movies_container?.removeChild(<HTMLElement>parent?.parentNode);
                }
            }
            );
        }
        else {
            console.error("Html element has not film_id attribute");
        }
    }
    else {
        svgs.forEach(svg => svg.setAttribute('fill', RED))
        if (id) {
            if (id_array) {
                new_id_array = JSON.parse(id_array);
                new_id_array.push(id)
            }
            else {
                new_id_array = [id];
            }
            window.localStorage.setItem('favourites', JSON.stringify(new_id_array))
            const film = await movieService.getMovieById(id);
            const film_card = domService.createFilmCard(film.poster_path, film.overview, film.release_date, +id, true);
            if (favorite_movies_container) {
                favorite_movies_container.innerHTML += film_card;
            }
            else {
                console.error('No html element with id favorite-movies')
            }
        }
    }
    AddEventListenerFavourite();
}

async function setFilms(category: Movie, page: number, search_string?: string): Promise<IMovies> {
    const previousCategory = movieService.nowCategory;
    let movies: IMovies = { results: [] };
    if (Movie.Search === category) {
        if (search_string) {
            movies = await movieService.getSearchMovies(page, search_string)
        }
    }
    else {
        movies = await movieService.getMovies(category, page);
    }
    const film_container = document.getElementById('film-container');
    DeleteEventListenerFavourite();
    if (film_container) {
        if (previousCategory !== category || page === 1) {
            film_container.innerHTML = "";
            history.pushState(null, "", '/');
        }
        if (movies.results?.length === 0) {
            film_container.innerHTML += "No films";
        }
        else {
            let film_cards = ''
            movies.results?.map(res => {
                const film_card = domService.createFilmCard(res.poster_path,
                    res.overview, res.release_date, res.id, false);
                film_cards += film_card;
            });
            film_container.innerHTML += film_cards;
        }
    }
    else {
        console.error("No html element with id film-container");
    }
    AddEventListenerFavourite();
    return movies;
}

function searchEvent(): void {
    const input = <HTMLInputElement>document.getElementById('search');
    const urlParams = new URLSearchParams(window.location.search);
    document.querySelectorAll("#button-wrapper input").forEach((element) => {
        (element as HTMLInputElement).checked = false;
    });
    setFilms(Movie.Search, 1, input?.value);
    history.pushState(null, "", "?" + urlParams.toString());
}

async function loadMore(): Promise<void> {
    const urlParams = new URLSearchParams(window.location.search);
    let page: string | null = urlParams.get('page');
    if (page) {
        page = (+page + 1).toString();
        urlParams.set('page', page);
    }
    else {
        page = '2';
        urlParams.set('page', page);
    }
    if (movieService.nowCategory === Movie.Search) {
        const input = <HTMLInputElement>document.getElementById('search');
        await setFilms(movieService.nowCategory, Number(page), input?.value);
    }
    else {
        await setFilms(movieService.nowCategory, Number(page));
    }

    history.pushState(null, "", "?" + urlParams.toString());
}

export {
    favouriteEvent,
    startFunction,
    setFilms,
    loadMore,
    searchEvent,
    AddEventListenerFavourite,
    DeleteEventListenerFavourite
}